package com.example.maxim.myinstagram.model;

import android.content.Context;
import android.content.SharedPreferences;

import com.example.maxim.myinstagram.App;
import com.example.maxim.myinstagram.Common;
import com.example.maxim.myinstagram.presenter.Presenter;

public class Model {
    private static final String PREFS = "prefs";
    private static final String THEME = "theme";

    private Presenter presenter;

    public Model(Presenter presenter){
        this.presenter = presenter;
    }

    public int getTheme() {
        SharedPreferences preferences =
                App.getInstance().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        int theme = preferences.getInt(THEME, Common.FIRST_THEME);
        return theme;
    }

    public void setTheme(int theme){
        SharedPreferences preferences =
                App.getInstance().getSharedPreferences(PREFS, Context.MODE_PRIVATE);
        preferences.edit()
                .putInt(THEME, theme)
                .apply();
    }
}
