package com.example.maxim.myinstagram.view;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.example.maxim.myinstagram.R;

public class MainFragment extends Fragment
                implements PhotoUpdateable{

    private static final String FROM_DB_FRAGMENT_TAG = "from_db";
    private static final String FROM_NET_FRAGMENT_TAG = "from_net";
    private static final String GENERAL_FRAGMENT_TAG = "general";


    private FrameLayout frameLayout;
    private FragmentManager manager;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView  = inflater.inflate(R.layout.main_fragment, container, false);

        frameLayout = (FrameLayout) rootView.findViewById(R.id.container_main_fragment);
        final FromDbFragment fromDbFragment = FromDbFragment.newInstance();
        final FromNetFragment fromNetFragment = FromNetFragment.newInstance();
        final GeneralFragment generalFragment = GeneralFragment.newInstance();

        manager = getActivity().getSupportFragmentManager();
        manager.beginTransaction()
                .replace(R.id.container_main_fragment, fromDbFragment, FROM_DB_FRAGMENT_TAG)
                .commit();

        BottomNavigationView bottomNavigationView =
                (BottomNavigationView) rootView.findViewById(R.id.bottom_nv_main_fragment);

        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                switch (menuItem.getItemId()){
                    case R.id.bottom_nv_menu_download_from_db:{
                        manager.beginTransaction()
                                .replace(R.id.container_main_fragment,
                                        fromDbFragment, FROM_DB_FRAGMENT_TAG)
                                .commit();
                        return true;
                    }
                    case R.id.bottom_nv_menu_download_from_net:{
                        manager.beginTransaction()
                                .replace(R.id.container_main_fragment,
                                        fromNetFragment, FROM_NET_FRAGMENT_TAG)
                                .commit();
                        return true;
                    }
                    case R.id.bottom_nv_menu_general:{
                        manager.beginTransaction()
                                .replace(R.id.container_main_fragment,
                                        generalFragment, GENERAL_FRAGMENT_TAG)
                                .commit();
                        MainActivity mainActivity = (MainActivity) getActivity();
                        mainActivity.updatePhotos();
                        return true;
                    }
                }
                return false;
            }
        });
        return rootView;
    }

    @Override
    public void updatePhotos() {
        Fragment fragment = manager.findFragmentByTag(GENERAL_FRAGMENT_TAG);
        if (fragment != null) {
            PhotoListFragment photoListFragment = (PhotoListFragment) fragment;
            photoListFragment.updatePhotos();
        }
    }
}


