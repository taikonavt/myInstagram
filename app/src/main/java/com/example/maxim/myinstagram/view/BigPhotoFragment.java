package com.example.maxim.myinstagram.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.maxim.myinstagram.R;

import java.io.File;

public class BigPhotoFragment extends Fragment {
    private static final String FILE_PATH_BIG_PHOTO = "file_name";

    public static BigPhotoFragment newInstance(File filePath) {
        BigPhotoFragment fragment = new BigPhotoFragment();

        Bundle args = new Bundle();
        args.putString(FILE_PATH_BIG_PHOTO, filePath.getAbsolutePath());
        fragment.setArguments(args);

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.big_photo_layout, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ImageView imageView = (ImageView) getView().findViewById(R.id.big_photo_image);
        Bitmap bitmap = BitmapFactory.decodeFile(getFilePath());
        imageView.setImageBitmap(bitmap);
    }

    private String getFilePath() {
        return getArguments().getString(FILE_PATH_BIG_PHOTO, "");
    }
}
