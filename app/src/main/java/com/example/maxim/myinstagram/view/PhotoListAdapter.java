package com.example.maxim.myinstagram.view;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;

import com.example.maxim.myinstagram.R;
import com.example.maxim.myinstagram.presenter.MainPresenter;

import java.io.File;
import java.util.ArrayList;

public class PhotoListAdapter extends RecyclerView.Adapter {
    private ArrayList<String> files;
    private PhotoListFragment fragment;

    PhotoListAdapter(PhotoListFragment fragment, ArrayList<String> files) {
        this.fragment = fragment;
        this.files = files;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        int reqWidth = fragment.getReqWidth();
        Bitmap bitmap = decodeSampledBitmap(files.get(i), reqWidth);
        ((MyViewHolder) viewHolder).imageView.setImageBitmap(bitmap);
        viewHolder.itemView.setTag(i);

        MainActivity activity = (MainActivity) fragment.getActivity();
        MainPresenter presenter = activity.getPresenter();
        File file = new File(files.get(i));
        if (presenter.fileIsFavourite(file)){
            ((MyViewHolder) viewHolder).star.setRating(1);
        } else {
            ((MyViewHolder) viewHolder).star.setRating(0);
        }
    }

    @Override
    public int getItemCount() {
        return files.size();
    }

    public void swap(ArrayList<String> files) {
        this.files = files;
    }

    private Bitmap decodeSampledBitmap(String path, int reqWidth){
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateSampleSize(options, reqWidth);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    private int calculateSampleSize(BitmapFactory.Options options, int reqWidth) {
        int width = options.outWidth;
        int inSampleSize = 1;

        if (width > reqWidth){
            int halfWidth = width / 2;
            while ((halfWidth / inSampleSize) > reqWidth){
                inSampleSize *= 2;
            }
        }
        return inSampleSize;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        ImageView imageView;
        RatingBar star;

        MyViewHolder(@NonNull final View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.item_image);
            star = (RatingBar) itemView.findViewById(R.id.item_star);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int id = (int) itemView.getTag();
                    final File file = new File(files.get(id));
                    fragment.onItemClick(file);
                }
            });

            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    int id = (int) itemView.getTag();
                    final File file = new File(files.get(id));
                    fragment.onItemLongClick(file);
                    return true;
                }
            });

            star.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                      @Override
                      public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                          if (b) {
                              int id = (int) itemView.getTag();
                              final File file = new File(files.get(id));
                              if (v == 0) {
                                  fragment.onStarUnchecked(file);
                              } else {
                                  fragment.onStarChecked(file);
                              }
                          }
                      }
                  }
            );
        }
    }

    interface OnItemClickListener{
        void onItemClick(File file);
    }


    interface OnItemLongClickListener{
        void onItemLongClick(File file);
    }


    interface OnStarClickListener {
        void onStarChecked(File file);
        void onStarUnchecked(File file);
    }
}
