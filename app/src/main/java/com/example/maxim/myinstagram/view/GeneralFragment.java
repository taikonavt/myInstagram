package com.example.maxim.myinstagram.view;

import java.util.ArrayList;

public class GeneralFragment extends PhotoListFragment {

        public static GeneralFragment newInstance() {
            return new GeneralFragment();
        }

        @Override
        ArrayList<String> getFileList() {
            MainActivity mainActivity = (MainActivity) getActivity();
            if (mainActivity != null) {
                return mainActivity.getPresenter().getFilesList();
            } else return null;
        }
}
