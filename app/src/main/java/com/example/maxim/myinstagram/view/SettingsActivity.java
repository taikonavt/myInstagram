package com.example.maxim.myinstagram.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.example.maxim.myinstagram.Common;
import com.example.maxim.myinstagram.R;
import com.example.maxim.myinstagram.model.Model;
import com.example.maxim.myinstagram.presenter.MainPresenter;
import com.example.maxim.myinstagram.presenter.Presenter;

public class SettingsActivity extends AppCompatActivity
    implements Presenter {

    private Toolbar toolbar;
    private Switch aSwitch;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (Common.theme == Common.SECOND_THEME){
            setTheme(R.style.AppThemeBlack);
        }
        super.onCreate(savedInstanceState);

        setContentView(R.layout.settings_layout);

        toolbar = findViewById(R.id.toolbar_settings);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        aSwitch = (Switch) findViewById(R.id.switch_color);

        final Model model = new Model(this);

        if (model.getTheme() == Common.SECOND_THEME) {
            aSwitch.setChecked(true);
        }

        aSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                MainPresenter.setThemeIsChanged(true);
                if (b) {
                    model.setTheme(Common.SECOND_THEME);
                } else {
                    model.setTheme(Common.FIRST_THEME);
                }

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home: {
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
