package com.example.maxim.myinstagram;

public class Common {
    public static final int FIRST_THEME = 1;
    public static final int SECOND_THEME = 2;

    public static int theme = FIRST_THEME;
    public static boolean themeIsChanged = false;
}
