package com.example.maxim.myinstagram.view;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.maxim.myinstagram.R;

import java.io.File;
import java.util.ArrayList;

public abstract class PhotoListFragment extends Fragment
        implements PhotoListAdapter.OnStarClickListener,
        PhotoListAdapter.OnItemClickListener,
        PhotoListAdapter.OnItemLongClickListener {

    public static final String FILE_PATH_DELETE_DIALOG = "file_name";
    private static final int REQUEST_C0DE_DIALOG_FRAGMENT = 2;
    private RecyclerView recyclerView;
    private MainActivity mainActivity;
    private PhotoListAdapter adapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.photo_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mainActivity = (MainActivity) getActivity();
        initView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_C0DE_DIALOG_FRAGMENT: {
                String path = data.getStringExtra(DeleteDialogFragment.DELETED_FILE);
                mainActivity.getPresenter().deletingConfirmed(path);
                break;
            }
        }
    }

    private void initView() {
        recyclerView = (RecyclerView) getView().findViewById(R.id.rv_photo_list_fragment);
        recyclerView.setHasFixedSize(true);
        GridLayoutManager gm = new GridLayoutManager(getContext(), 2);
        recyclerView.setLayoutManager(gm);
        adapter = new PhotoListAdapter(this, getFileList());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onStarChecked(File file) {
        mainActivity.getPresenter().onStarChecked(file);
    }

    @Override
    public void onStarUnchecked(File file) {
        mainActivity.getPresenter().onStarUnchecked(file);
    }

    @Override
    public void onItemClick(File file) {
        mainActivity.getPresenter().onItemClick(file);
    }

    @Override
    public void onItemLongClick(File file) {
        mainActivity.getPresenter().onItemLongClick(file.getAbsolutePath());
    }

    public void showDeletePhotoError() {
        View view = getView();
        if (view != null) {
            Snackbar.make(view, getString(R.string.photo_deleting_error),
                    Snackbar.LENGTH_LONG).show();
        }
    }

    public int getReqWidth() {
        Display display = getActivity().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int reqWidth = width / 2;
        return reqWidth;
    }

    public void showDeleteDialog(String path) {
        DeleteDialogFragment deleteDialog = new DeleteDialogFragment();
        deleteDialog.setTargetFragment(this, REQUEST_C0DE_DIALOG_FRAGMENT);
        Bundle bundle = new Bundle();
        bundle.putString(FILE_PATH_DELETE_DIALOG, path);
        deleteDialog.setArguments(bundle);
        FragmentManager manager = getFragmentManager();
        if (manager != null) {
            deleteDialog.show(manager, "DeleteDialog");
        }
    }

    public void updatePhotos() {
        adapter.swap(getFileList());
        adapter.notifyDataSetChanged();
    }

    abstract ArrayList<String> getFileList();
}
