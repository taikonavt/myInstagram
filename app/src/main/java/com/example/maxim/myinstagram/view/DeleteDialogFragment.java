package com.example.maxim.myinstagram.view;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialogFragment;

import com.example.maxim.myinstagram.R;

public class DeleteDialogFragment extends AppCompatDialogFragment
implements DialogInterface.OnClickListener {

    public static final String DELETED_FILE = "ok";

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder adb = new AlertDialog.Builder(getActivity())
                .setTitle(R.string.delete_title)
                .setPositiveButton(R.string.delete_positive, this)
                .setNegativeButton(R.string.delete_negative, this)
                .setMessage(R.string.delete_message);
        return adb.create();
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        switch (i){
            case DialogInterface.BUTTON_POSITIVE:{
                Bundle bundle = getArguments();
                String path = bundle.getString(PhotoListFragment.FILE_PATH_DELETE_DIALOG);
                Intent intent = new Intent();
                intent.putExtra(DELETED_FILE, path);
                getTargetFragment().onActivityResult(
                        getTargetRequestCode(), Activity.RESULT_OK, intent);
                break;
            }
            case DialogInterface.BUTTON_NEGATIVE:{
                break;
            }
        }
    }
}
