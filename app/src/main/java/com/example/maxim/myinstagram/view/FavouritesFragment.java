package com.example.maxim.myinstagram.view;

import java.util.ArrayList;

public class FavouritesFragment extends PhotoListFragment
                implements PhotoUpdateable {

    public static FavouritesFragment newInstance() {
        return new FavouritesFragment();
    }

    @Override
    ArrayList<String> getFileList() {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            return mainActivity.getPresenter().getFavouritesFilesList();
        } else return null;
    }

    @Override
    public void updatePhotos() {
        super.updatePhotos();
    }


//    public void updateFavouritePhotos(){
//        for (int i = 0; i < photoListPagerAdapter.getCount(); i++) {
//            CharSequence title = photoListPagerAdapter.getPageTitle(i);
//            if (title != null) {
//                if (title.equals(getString(R.string.tab_favourite))){
//                    PhotoListFragment fragment = (PhotoListFragment) photoListPagerAdapter.getItem(i);
//                    fragment.updatePhotos();
//                }
//            }
//        }
//    }
}
