package com.example.maxim.myinstagram.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.example.maxim.myinstagram.Common;
import com.example.maxim.myinstagram.R;
import com.example.maxim.myinstagram.presenter.MainPresenter;

import java.io.File;

public class MainActivity extends AppCompatActivity
    implements NavigationView.OnNavigationItemSelectedListener{

    public static final String TAG = "myTag";
    private static final int REQUEST_CODE_PHOTO = 1;

    private DrawerLayout drawer;
    private MainPresenter presenter;
    private Toolbar toolbar;
    private NavigationView navigationView;
    private FloatingActionButton fab;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private PhotoListPagerAdapter photoListPagerAdapter;
    private FrameLayout frameLayout;
    private AppBarLayout appBarLayout;
    private CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new MainPresenter(this);
        if (Common.theme == Common.SECOND_THEME){
            setTheme(R.style.AppThemeBlack);
        }
        setContentView(R.layout.main_layout);

//        if (savedInstanceState == null) {
//            getSupportFragmentManager().beginTransaction()
//                    .replace(R.id.container, MainFragment.newInstance())
//                    .commitNow();
//        }
        initView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (Common.themeIsChanged) {
            Common.themeIsChanged = false;
            recreate();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.detachView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        drawer.closeDrawer(GravityCompat.START);
        switch (menuItem.getItemId()){
            case R.id.nav_photos: {

                break;
            }
            case R.id.nav_settings: {
                presenter.navSettingsClicked();
                break;
            }
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CODE_PHOTO: {
                presenter.photoGot();
                break;
            }
        }
    }

    private void initView() {
        toolbar = findViewById(R.id.toolbar_app_bar_main);
        setSupportActionBar(toolbar);

        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_main);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_main);

        drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.nav_drawer_open, R.string.nav_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        fab = findViewById(R.id.fab_app_bar_main);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                presenter.onFabClick();
            }
        });

        frameLayout = (FrameLayout) findViewById(R.id.container_frame_app_bar_main);

        viewPager = (ViewPager) findViewById(R.id.container_vp_app_bar_main);

        MainFragment mainFragment = MainFragment.newInstance();
        FavouritesFragment favouritesFragment = FavouritesFragment.newInstance();

        photoListPagerAdapter = new PhotoListPagerAdapter(getSupportFragmentManager());
        photoListPagerAdapter.addFragment(mainFragment, getString(R.string.tab_main));
        photoListPagerAdapter.addFragment(favouritesFragment, getString(R.string.tab_favourite));
        viewPager.setAdapter(photoListPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.tabs_app_bar_main);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public void onBackPressed() {
        if (frameLayout.getVisibility() == View.VISIBLE) {
            prepareViewForTabs();
            hideBigPhoto();
        } else {
            super.onBackPressed();
        }
    }

    public void showBigPhoto(File file) {
        prepareViewForBigPhoto();
        BigPhotoFragment fragment = BigPhotoFragment.newInstance(file);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container_frame_app_bar_main, fragment)
                .commit();
    }

    private void prepareViewForBigPhoto() {
        appBarLayout.setExpanded(false);
        fab.hide();
        viewPager.setVisibility(View.INVISIBLE);
        tabLayout.setVisibility(View.INVISIBLE);
        frameLayout.setVisibility(View.VISIBLE);
    }

    private void prepareViewForTabs() {
        appBarLayout.setExpanded(true);
        fab.show();
        frameLayout.setVisibility(View.INVISIBLE);
        tabLayout.setVisibility(View.VISIBLE);
        viewPager.setVisibility(View.VISIBLE);
    }

    private void hideBigPhoto() {
        Fragment fragment =
                getSupportFragmentManager().findFragmentById(R.id.container_frame_app_bar_main);
        getSupportFragmentManager().beginTransaction()
                .remove(fragment)
                .commit();
    }

    public void startSettings() {
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivity(intent);
    }

    public void setMyTheme(int theme){
        Common.theme = theme;
    }

    public void makePhoto(Uri photoUri) {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
        startActivityForResult(intent, REQUEST_CODE_PHOTO);
    }

    public void updatePhotos() {
        for (int i = 0; i < photoListPagerAdapter.getCount(); i++) {
            Fragment fragment = photoListPagerAdapter.getItem(i);
            PhotoUpdateable photoUpdateable = (PhotoUpdateable) fragment;
            photoUpdateable.updatePhotos();
        }
    }

    public MainPresenter getPresenter() {
        return presenter;
    }

    public void showDeleteDialog(String absolutePath) {
        PhotoListFragment fragment = (PhotoListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container_vp_app_bar_main);
        if (fragment != null) {
            fragment.showDeleteDialog(absolutePath);
        }
    }

    public void showDeletePhotoError() {
        PhotoListFragment fragment = (PhotoListFragment) getSupportFragmentManager()
                .findFragmentById(R.id.container_vp_app_bar_main);
        if (fragment != null) {
            fragment.showDeletePhotoError();
        }
    }

    public void showMarkFavouriteError(File file) {
        View view = findViewById(R.id.coordinator_main);
        Snackbar.make(view, getString(R.string.photo_favouriting_error),
                Snackbar.LENGTH_LONG).show();
    }
}
