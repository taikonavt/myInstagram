package com.example.maxim.myinstagram.view;

public interface PhotoUpdateable {
    void updatePhotos();
}
