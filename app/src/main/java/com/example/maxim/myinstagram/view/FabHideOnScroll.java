package com.example.maxim.myinstagram.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

public class FabHideOnScroll extends FloatingActionButton.Behavior {

    public FabHideOnScroll(Context context, AttributeSet attrs) {
        super();
    }

    @Override
    public void onNestedPreScroll(
            @NonNull CoordinatorLayout coordinatorLayout,
            @NonNull FloatingActionButton child,
            @NonNull View target,
            int dx, int dy,
            @NonNull int[] consumed,
            int type) {
        super.onNestedPreScroll(coordinatorLayout, child, target, dx, dy, consumed, type);

        if (dy > 0) {
            CoordinatorLayout.LayoutParams layoutParams =
                    (CoordinatorLayout.LayoutParams) child.getLayoutParams();
            int fabBottomMargin = layoutParams.bottomMargin;
            child.animate().translationY(child.getHeight() +
                    fabBottomMargin).setInterpolator(new LinearInterpolator()).start();
        } else if (dy < 0) {
            child.animate().translationY(0).setInterpolator(new LinearInterpolator()).start();
        }

//        if (child.getVisibility() == View.VISIBLE && dy > 0) {
//            child.setVisibility(View.INVISIBLE);
////            hide(); не подходит т.к. если fab is GONE, кнопка перестает получать события при
//// прокручивании
//        } else if (child.getVisibility() == View.INVISIBLE && dy < 0) {
//            child.setVisibility(View.VISIBLE);
//        }
    }

    @Override
    public boolean onStartNestedScroll(
            @NonNull CoordinatorLayout coordinatorLayout,
            @NonNull FloatingActionButton child,
            @NonNull View directTargetChild,
            @NonNull View target, int axes, int type) {
        return axes == ViewCompat.SCROLL_AXIS_VERTICAL;
    }
}
