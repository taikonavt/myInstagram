package com.example.maxim.myinstagram.presenter;

import android.net.Uri;
import android.os.Environment;

import com.example.maxim.myinstagram.Common;
import com.example.maxim.myinstagram.model.Model;
import com.example.maxim.myinstagram.view.MainActivity;
import com.example.maxim.myinstagram.view.PhotoListFragment;

import java.io.File;
import java.util.ArrayList;

public class MainPresenter implements Presenter{
    private static final String FAVOURITE_MARK = "_fav";

    private MainActivity activity;
    private PhotoListFragment photoListFragment;
    private Model model;

    public MainPresenter(MainActivity activity){
        this.activity = activity;
        model = new Model(this);
        activity.setMyTheme(model.getTheme());
    }

    public void attachView(MainActivity mainActivity) {
        activity = mainActivity;
    }

    public void detachView() {
        activity = null;
    }

    public void navSettingsClicked(){
        activity.startSettings();
    }

    public static void setThemeIsChanged(boolean isChanged) {
        Common.themeIsChanged = isChanged;
    }

    public void onFabClick() {
        activity.makePhoto(generateFileUri());
    }

    private Uri generateFileUri() {
        File directory = getDirectory();
        File file = new File(directory.getPath() + "/" + "photo_"
                + System.currentTimeMillis() + ".jpg");
        return Uri.fromFile(file);
    }

    private File getDirectory() {
        File directory = new File(
                activity.getApplicationContext()
                        .getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                        .toString());
        if (!directory.exists())
            directory.mkdirs();
        return directory;
    }

    public void photoGot() {
        activity.updatePhotos();
    }

//    private File getDirectory(){
//        File directory = new File(
//                fragment.getActivity()
//                        .getApplicationContext()
//                        .getExternalFilesDir(Environment.DIRECTORY_PICTURES)
//                        .toString());
//        if (!directory.exists())
//            directory.mkdirs();
//        return directory;
//    }

    public ArrayList<String> getFilesList() {
        File directory = getDirectory();
        File[] files = directory.listFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < files.length; i++) {
            arrayList.add(files[i].getAbsolutePath());
        }
        return arrayList;
    }

    public ArrayList<String> getFavouritesFilesList() {
        File directory = getDirectory();
        File[] files = directory.listFiles();
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < files.length; i++) {
            if (files[i].getAbsolutePath().contains(FAVOURITE_MARK)) {
                arrayList.add(files[i].getAbsolutePath());
            }
        }
        return arrayList;
    }

    public void onItemClick(File file) {
        activity.showBigPhoto(file);
    }

    public void onItemLongClick(String absolutePath) {
        activity.showDeleteDialog(absolutePath);
    }

    public void deletingConfirmed(String path) {
        deletePhoto(path);
    }

    private void deletePhoto(String path) {
        File file = new File(path);
        if (!file.delete())
            activity.showDeletePhotoError();
        activity.updatePhotos();
    }

    public void onStarChecked(File file) {
        markFileAsFavourite(file);
        activity.updatePhotos();
    }

    public void onStarUnchecked(File file) {
        markFileAsSimple(file);
        activity.updatePhotos();
    }

    private void markFileAsFavourite(File file){
        String oldName = file.getAbsolutePath();
        int pos = oldName.lastIndexOf(".");
        String extension = oldName.substring(pos);
        String newName = oldName.replace(extension, FAVOURITE_MARK + extension);
        File markedFile = new File(newName);
        boolean flag = false;
        if (markedFile.exists()){
            activity.showMarkFavouriteError(file);
        }
        else {
            flag = file.renameTo(markedFile);
        }
    }

    private void markFileAsSimple(File file){
        String oldName = file.getAbsolutePath();
        String newName = oldName.replace(FAVOURITE_MARK, "");
        File simpleFile = new File(newName);
        boolean flag = false;
        if (simpleFile.exists()) {
            activity.showMarkFavouriteError(file);
        } else {
            flag = file.renameTo(simpleFile);
        }
    }

    public boolean fileIsFavourite(File file){
        String name = file.getName();
        boolean flag = name.contains(FAVOURITE_MARK);
        return flag;
    }
}
