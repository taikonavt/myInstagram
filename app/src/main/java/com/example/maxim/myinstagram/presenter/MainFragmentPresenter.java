package com.example.maxim.myinstagram.presenter;

import com.example.maxim.myinstagram.view.MainFragment;

public class MainFragmentPresenter {

    private MainFragment fragment;

    public void attachView(MainFragment mainFragment) {
        fragment = mainFragment;
    }

    public void detachView(){
        fragment = null;
    }


}
